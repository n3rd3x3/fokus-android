package com.isaiahvonrundstedt.fokus.features.subject

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.isaiahvonrundstedt.fokus.R

@Composable
fun SubjectScreenLayout() {
    Text(stringResource(id = R.string.navigation_subjects))
}
